package main

// Publisher contains apps published by a publisher
type Publisher struct {
	ID   string          `json:"publisherID"`
	Apps map[string]bool `json:"apps"`
}

func getPublishers(apps chan App) map[string]Publisher {
	var (
		store = make(map[string]Publisher)
		j     = 0
	)

	for app := range apps {
		j++
		if j%15000 == 0 {
			println(j)
		}

		pub, ok := store[app.PublisherID]
		if !ok {
			pub = Publisher{
				ID:   app.PublisherID,
				Apps: make(map[string]bool),
			}
		}
		pub.Apps[app.URL] = true
		store[pub.ID] = pub
	}

	return store
}

func (pub *Publisher) isWhiteListed(whitelisted []string) bool {
	var publisher string

	if containsLetter(pub.ID) {
		publisher = "https://play.google.com/store/apps/developer?id=" + pub.ID
	} else {
		publisher = "https://play.google.com/store/apps/dev?id=" + pub.ID
	}

	return isWhiteListed(publisher, whitelisted)
}
