package main

import (
	"errors"
	"time"
)

// GGStats provides statistical analysis for playstore data.
type GGStats interface {
	UpdateInfo(time.Time) (int, int, error)
	ReviewInfo(int) (int, int)
	GenreInfo() map[string]int
	AppReport(string) (App, []App, error)
}

// ggStats is a concrete implementation of GGStats
type ggStats struct {
	Apps            map[string]App
	Publishers      map[string]Publisher
	CommentKeywords []string
}

func (svc *ggStats) UpdateInfo(s time.Time) (int, int, error) {
	var (
		appCount int
		pubCount int
	)
	for _, app := range svc.Apps {
		if app.LastUpdated.After(s) {
			appCount++
		}
	}
	for _, pub := range svc.Publishers {
		for appURL := range pub.Apps {
			if svc.Apps[appURL].LastUpdated.After(s) {
				pubCount++
				break
			}
		}
	}
	return appCount, pubCount, nil
}

func (svc *ggStats) ReviewInfo(stars int) (int, int) {
	var (
		whitelistedComments int
		totalComments       int
	)

	for _, app := range svc.Apps {
		for _, review := range app.Reviews {
			if review.Rating == stars {
				if containsKeywords(review, svc.CommentKeywords) {
					whitelistedComments++
				}
				totalComments++
			}
		}
	}

	return whitelistedComments, totalComments
}

func (svc *ggStats) GenreInfo() map[string]int {
	genres := make(map[string]int)
	for _, app := range svc.Apps {
		if _, ok := genres[app.Genre]; ok {
			genres[app.Genre]++
		} else {
			genres[app.Genre] = 1
		}
	}
	return genres
}

func (svc *ggStats) AppReport(url string) (App, []App, error) {
	var (
		whitelistedComments int
		totalComments       int
		pubApps             []App
	)
	app, ok := svc.Apps[url]
	if !ok {
		return app, pubApps, errors.New("App not present in Database")
	}

	for appURL := range svc.Publishers[app.PublisherID].Apps {
		pubApps = append(pubApps, svc.Apps[appURL])
	}

	for i, pubApp := range pubApps {
		whitelistedComments = 0
		totalComments = 0
		for _, review := range pubApp.Reviews {
			if containsKeywords(review, svc.CommentKeywords) {
				whitelistedComments++
			}
			totalComments++
		}
		pubApp.WhitelistedComments = whitelistedComments
		pubApp.TotalComments = totalComments
		pubApps[i] = pubApp
		if pubApp.URL == app.URL {
			app = pubApp
		}
	}
	return app, pubApps, nil
}

func newGGStatsService() GGStats {
	chanApps := make(chan App, 1000)
	var apps map[string]App
	go func() {
		apps = getApps(readCSV("out.csv"), chanApps)
	}()

	println("getting pubs")
	publishers := getPublishers(chanApps)

	apps = getReviews(readCSV("reviews.csv"), apps)

	svc := &ggStats{
		Apps:            apps,
		Publishers:      publishers,
		CommentKeywords: []string{"ads", "ad", "advertisement", "adds", "anzeigen", "publicités"},
	}
	return svc
}
