package main

import (
	"encoding/csv"
	"os"
	"strings"
)

func readCSV(filename string) [][]string {
	f, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	lines, err := csv.NewReader(f).ReadAll()
	if err != nil {
		panic(err)
	}
	return lines
}

func containsLetter(s string) bool {
	for _, x := range s {
		if x > '9' || x < '0' {
			return true
		}
	}
	return false
}

func containsKeywords(review Review, keywords []string) bool {
	for _, sword := range strings.Split(review.Comment, " ") {
		for _, word := range keywords {
			if sword == word {
				return true
			}
		}
	}
	return false
}
