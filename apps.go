package main

import (
	"strconv"
	"strings"
	"time"
)

const (
	jsRating   = `Array.from(document.getElementsByClassName("nt2C1d")).map(a => a.getElementsByClassName('pf5lIe')[0]).map(a => a.getElementsByTagName('div')[0]).map(a => a.getAttribute("aria-label"));`
	jsComments = `Array.from(document.getElementsByClassName("UD7Dzf")).map(a => a.getElementsByTagName('span')[0]).map(a => a.innerText);`
)

// App contains information available for an app
type App struct {
	Name                string    `json:"name"`
	Genre               string    `json:"Genre"`
	Email               string    `json:"Email"`
	URL                 string    `json:"url"`
	Ratings             float64   `json:"ratings"`
	PublisherID         string    `json:"publisher_id"`
	Installs            int64     `json:"installs"`
	LastUpdated         time.Time `json:"last_updated"`
	WhitelistedComments int
	TotalComments       int
	Reviews             []Review
}

func getApps(lines [][]string, apps chan App) map[string]App {
	appArray := make(map[string]App)
	for _, line := range lines {
		rating, _ := strconv.ParseFloat(line[1], 64)
		installs, _ := strconv.ParseInt(strings.Join(strings.Split(strings.Split(line[5], " ")[0], ","), ""), 10, 64)
		lastUpdated, _ := time.Parse("January 2, 2006", line[9])

		app := App{
			Name:        line[0],
			Ratings:     rating,
			PublisherID: line[4],
			Installs:    installs,
			Genre:       line[6],
			Email:       line[8],
			LastUpdated: lastUpdated,
			URL:         line[7],
		}
		apps <- app
		appArray[app.URL] = app
	}
	println("data read")
	defer close(apps)
	return appArray
}
