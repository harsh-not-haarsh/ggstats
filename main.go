package main

import (
	"log"
	"net/http"

	httptransport "github.com/go-kit/kit/transport/http"
)

func main() {
	svc := newGGStatsService()

	updateInfoHandler := httptransport.NewServer(
		makeUpdateInfoEndpoint(svc),
		decodeUpdateInfoRequest,
		encodeResponse,
	)

	reviewInfoHandler := httptransport.NewServer(
		makeReviewInfoEndpoint(svc),
		decodeReviewInfoRequest,
		encodeResponse,
	)

	genreInfoHandler := httptransport.NewServer(
		makeGenreInfoEndpoint(svc),
		decodeGenreInfoRequest,
		encodeResponse,
	)

	appReportHandler := httptransport.NewServer(
		makeAppReportEndpoint(svc),
		decodeAppReportRequest,
		encodeResponse,
	)

	http.Handle("/updateInfo", updateInfoHandler)
	http.Handle("/reviewInfo", reviewInfoHandler)
	http.Handle("/genreInfo", genreInfoHandler)
	http.Handle("/appReport", appReportHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
