package main

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"github.com/go-kit/kit/endpoint"
)

type updateInfoRequest struct {
	AfterDate string `json:"after_date"`
}

type updateInfoResponse struct {
	AppCount int    `json:"app_count"`
	PubCount int    `json:"pub_count"`
	Err      string `json:"err,omitempty"`
}

type reviewInfoRequest struct {
	Stars int `json:"stars"`
}

type reviewInfoResponse struct {
	Whitelisted int `json:"whitelisted"`
	Total       int `json:"total"`
}

type genreInfoResponse struct {
	Genre map[string]int `json:"genre"`
}

type appReportRequest struct {
	AppURL string `json:"app_url"`
}

type appReportResponse struct {
	App       App    `json:"app"`
	Publisher []App  `json:"publisher_apps"`
	Err       string `json:"err"`
}

func makeUpdateInfoEndpoint(svc GGStats) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(updateInfoRequest)
		out, err := time.Parse("2006-01-02", req.AfterDate)
		if err != nil {
			return updateInfoResponse{}, err
		}
		appCount, pubCount, err := svc.UpdateInfo(out)
		if err != nil {
			return updateInfoResponse{appCount, pubCount, err.Error()}, nil
		}
		return updateInfoResponse{appCount, pubCount, ""}, nil
	}
}

func makeReviewInfoEndpoint(svc GGStats) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(reviewInfoRequest)
		whitelistedComments, totalComments := svc.ReviewInfo(req.Stars)
		return reviewInfoResponse{whitelistedComments, totalComments}, nil
	}
}

func makeGenreInfoEndpoint(svc GGStats) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		// req := request.(publisherInfoRequest)
		publishers := svc.GenreInfo()
		return genreInfoResponse{publishers}, nil
	}
}

func makeAppReportEndpoint(svc GGStats) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(appReportRequest)
		app, pubApps, err := svc.AppReport(req.AppURL)
		if err == nil {
			return appReportResponse{app, pubApps, err.Error()}, nil
		}
		return appReportResponse{app, pubApps, ""}, nil
	}
}

func decodeUpdateInfoRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request updateInfoRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeReviewInfoRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request reviewInfoRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeGenreInfoRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request reviewInfoRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeAppReportRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request appReportRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}
