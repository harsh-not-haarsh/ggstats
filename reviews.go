package main

import (
	"strconv"
	"strings"
	"time"
)

// Review contains info of a review for an app
type Review struct {
	Comment string    `json:"text"`
	Rating  int       `json:"rating"`
	Date    time.Time `json:"date"`
	URL     string    `json:"url"`
}

func getReviews(lines [][]string, apps map[string]App) map[string]App {
	for _, line := range lines {
		rating, _ := strconv.Atoi(line[1])
		date, _ := time.Parse("2006-01-02", line[2])

		review := Review{
			Comment: strings.ToLower(line[0]),
			Rating:  rating,
			Date:    date,
			URL:     line[3],
		}
		app := apps[review.URL]
		app.Reviews = append(app.Reviews, review)
		apps[review.URL] = app
	}

	return apps
}
